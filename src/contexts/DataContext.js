import React, { createContext, useState, useEffect } from "react";
export const DataContext = createContext();

const DataContextProvider = (props) => {
  const [data, setData] = useState({});
  const [hasError, setErrors] = useState(false);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json`);
      res
        .json()
        .then((res) => setData(res))
        .catch((err) => setErrors(err));
    }

    fetchData();
  }, []);

  return (
    <DataContext.Provider value={{ data }}>
      {props.children}
    </DataContext.Provider>
  );
};

export default DataContextProvider;
