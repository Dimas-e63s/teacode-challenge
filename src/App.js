import './index.css';
import DataContextProvider from "./contexts/DataContext";
import ContactsList from './components/ContactsList'

function App() {
  return (
    <div className="App">
      <DataContextProvider>
        <ContactsList></ContactsList>
      </DataContextProvider>
    </div>
  );
}

export default App;
