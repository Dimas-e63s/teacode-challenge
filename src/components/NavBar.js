import '../index.css';

const Navbar = ({filterText, onFilterTextChange}) => {
  return (
    <div className="navbar">
      <header>
        Contacts
      </header>
      <form>
        <input
          className="search-input"
          type="text"
          placeholder="Search..."
          value={filterText}
          onChange={e => onFilterTextChange(e.target.value)}
        />
      </form>
    </div>
  );
}

export default Navbar;