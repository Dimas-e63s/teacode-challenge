import React from "react";
import '../index.css';

const DataDetails = ({ iterator, checked, onHandleChange}) => {
  const clickHandler = e => {
    const checkedVal =
      e.target.type === "checkbox"
        ? e.target.checked
        : !e.target.querySelector("input").checked;

    const payload = {
      id: iterator.id,
      value: checkedVal
    };
    onHandleChange(payload);
  }
  return (
    <div className="user-container" onClick={clickHandler}>
      <img src={iterator.avatar} alt=""/>
      {`${iterator.first_name} ${iterator.last_name}`}
      <input type="checkbox" checked={checked} onChange={clickHandler}/>
    </div>
  )
};

export default DataDetails;