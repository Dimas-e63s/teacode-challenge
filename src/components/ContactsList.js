import React, { useContext, useState } from "react";
import Navbar from './NavBar';
import DataLoop from './DataLoop';
import { DataContext } from "../contexts/DataContext";

const FilterableProductTable = () => {
    const { data } = useContext(DataContext);
    const [filterText, setFilterText] = useState('')

    const handleFilterChange = (text) => {
      setFilterText(text)
    }
  
    return (
      <div>
       <Navbar
          filterText={filterText}
          onFilterTextChange={handleFilterChange}
        />
        <DataLoop
          items={data}
          filterText={filterText}
        />
      </div>
    );
}

export default FilterableProductTable;