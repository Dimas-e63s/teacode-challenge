import React, { useState } from "react";
import DataDetails from "./DataDetails";

export default function DataLoop({items, filterText}) {
  const [checkedList, setCheckedList] = useState({})

  const handleContactChange = ({id, value}) => {
    setCheckedList((prev) => ({
      ...prev,
      [id]: value
    }));
  };

  const log = () => {
    const obj = Object.keys(checkedList).filter(i => checkedList[i])
    console.log('All selected items IDs', obj);
  }
  log()

  return items.length ? (
    <div>
      {items
        .sort((a,b) => (a.last_name > b.last_name) ? 1 : ((b.last_name > a.last_name) ? -1 : 0))
        .filter(item => {
          if(filterText !== '') {
            const {first_name, last_name} = item
            return first_name.includes(filterText) || last_name.includes(filterText) 
            ? true 
            : false
          }
          return item
        })
        .map(item => 
        <DataDetails 
          checked={Boolean(checkedList[item.id])} 
          key={item.id} 
          iterator={item}
          onHandleChange={handleContactChange}
        ></DataDetails>)}
    </div>
  ): <div className="empty">No Users</div>
}

